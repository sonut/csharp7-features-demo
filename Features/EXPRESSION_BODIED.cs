using System;
using static System.Console;

namespace Features
{
    public static class EXPRESSION_BODIED
    {
        public static void Demo()
        {
            var samples = new Samples();
            WriteLine($"Readonly Property Expression: { samples.FullName }");
            samples.X = "Hello Tom!";
            WriteLine($"Property Expression: { samples.X }");
            samples = null;
            GC.Collect();
        }

        private static void handler(Object s, EventArgs e)
        {
            WriteLine("Hi, I'm an event handler");
        }
    }

    public class Samples
    {
        public Samples() => WriteLine($"ctor Expression: {nameof(Samples)}");

        ~Samples() => WriteLine($"destructor Expression: {nameof(Samples)}");
        private string _FirstName = "Markus";
        private string _LastName = "Suulla";
        public string FullName => _FirstName + " " + _LastName;

        private string _x;
        public string X
        {
            get => _x;
            set => _x = value;
        }
        private EventHandler _someEvent;
        public event EventHandler SomeEvent
        {
            add => _someEvent += value;
            remove => _someEvent -= value;
        }
    }
}
using System.Threading.Tasks;
using static System.Console;

namespace Features
{
    public static class ASYNC_OPTIMZATION
    {
        // Generalized
        public static async Task GeneralizedType()
        {
            var foo = new Foo();
            var asyncResult = await foo.BarAsync();
            WriteLine(asyncResult);

            var syncResult = foo.BarSync();
            WriteLine(syncResult);
        }
    }

    interface IFoo
    {
        ValueTask<string> BarAsync();
        ValueTask<string> BarSync();
    }

    public class Foo : IFoo
    {
        public async ValueTask<string> BarAsync()
        {
            await Task.Delay(10);
            return "value";
        }

        public ValueTask<string> BarSync()
        {
            return new ValueTask<string>("value");
        }
    }
}
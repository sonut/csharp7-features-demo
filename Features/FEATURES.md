
C#7 Features : https://docs.microsoft.com/en-us/dotnet/articles/csharp/whats-new/csharp-7

2# TUPLES
   - Nuget package => dotnet add package System.ValueTuple
   - Tuples were available before C# 7, but had many limitations. Most importantly, the members of these tuples were named        Item1, Item2 and so on. The language support enables semantic names for the fields of a Tuple.

   - System.Tuple is a reference type and has cost of allocation and GC pressure, on other side System.ValueTuple is struct       type and have no allocation cost and GC pressure. it’s a mutable one, and one has to be careful when using them as such. 


4# Ref locals and returns
   - There are few restrictions with ref locals and ref returns
     1) You can’t return reference of anything and everything but to those fields in your objects, which are safe to return.
     2) Ref locals can’t be muted or changed to a point to another memory location as they are initialized to a certain memory     location.

7# Generalized async return types
   - Nuget package => dotnet add package System.Threading.Tasks.Extensions

   - Up until now, async methods in C# must either return void, Task or Task<T>. C# 7.0 allows other types to be defined in       such a way that they can be returned from an async method.
   
     For instance we plan to have a ValueTask<T> struct type. It is built to prevent the allocation of a Task<T> object in cases where the result of the async operation is already available at the time of awaiting. For many async scenarios where buffering is involved for example, this can drastically reduce the number of allocations and lead to significant performance gains.

   - Task<T> is a class and causes the unnecessary overhead of its allocation when the result is immediately available.
     ValueTask<T> is a structure and has been introduced to prevent the allocation of a Task object in case the result of the  async operation is already available at the time of awaiting.

Proposals:
   - Immutable Types
   - Record Types
   - Non-Nullable Ref
            int objNullVal;        //non-nullable value type
            int? objNotNullVal;    //nullable value type
            string!objNotNullRef;  //non-nullable reference type
            string objNullRef;     //nullable reference type 
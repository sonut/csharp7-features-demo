using System;
using System.Collections.Generic;

namespace Features
{
    public static class LOCAL_FUNCTIONS
    {

        // VERSION1
        public static IEnumerable<char> IteratorMethod_V1(char start, char end)
        {
            if ((start < 'a') || (start > 'z'))
                throw new ArgumentOutOfRangeException(paramName: nameof(start), message: "start must be a letter");
            if ((end < 'a') || (end > 'z'))
                throw new ArgumentOutOfRangeException(paramName: nameof(end), message: "end must be a letter");

            if (end <= start)
                throw new ArgumentException($"{nameof(end)} must be greater than {nameof(start)}");
            for (var c = start; c < end; c++)
                yield return c;
        }


        // VERSION2
        public static IEnumerable<char> IteratorMethod_V2(char start, char end)
        {
            if ((start < 'a') || (start > 'z'))
                throw new ArgumentOutOfRangeException(paramName: nameof(start), message: "start must be a letter");
            if ((end < 'a') || (end > 'z'))
                throw new ArgumentOutOfRangeException(paramName: nameof(end), message: "end must be a letter");

            if (end <= start)
                throw new ArgumentException($"{nameof(end)} must be greater than {nameof(start)}");
            return alphabetSubsetImplementation(start, end);
        }

        private static IEnumerable<char> alphabetSubsetImplementation(char start, char end)
        {
            for (var c = start; c < end; c++)
                yield return c;
        }


        // VERSION3
        public static IEnumerable<char> IteratorMethod_V3(char start, char end)
        {
            if ((start < 'a') || (start > 'z'))
                throw new ArgumentOutOfRangeException(paramName: nameof(start), message: "start must be a letter");
            if ((end < 'a') || (end > 'z'))
                throw new ArgumentOutOfRangeException(paramName: nameof(end), message: "end must be a letter");

            if (end <= start)
                throw new ArgumentException($"{nameof(end)} must be greater than {nameof(start)}");

            return alphabetSubsetImplementation();

            IEnumerable<char> alphabetSubsetImplementation()
            {
                for (var c = start; c < end; c++)
                    yield return c;
            }
        }
    }
}
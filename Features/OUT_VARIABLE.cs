using static System.Console;

namespace Features
{
    public static class OUT_VARIABLES
    {
        public static void OutVariable(string input)
        {
            if (int.TryParse(input, out int result))
                WriteLine(result);
            else
                WriteLine("Could not parse input");
        }
    }
}
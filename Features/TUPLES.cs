using System.Collections.Generic;
using static System.Console;

namespace Features
{
    public static class TUPLES
    {
        public static void NamedTuples()
        {
            var letters = ("a", "b");
            WriteLine(letters.Item1);
            WriteLine(letters.Item2);

            (string Alpha, string Beta) namedLetters = ("a", "b");
            // var namedLetters = (Alpha: "a", Beta: "b");

            WriteLine(namedLetters.Alpha);
            WriteLine(namedLetters.Beta);
        }

        public static void TupleAsMethodReturnType()
        {
            var result = Range(new List<int> { 10, 39, 123, 43, 21, 88 });
            WriteLine($"Min: {result.Min}");
            WriteLine($"Max: {result.Max}");

            (int Max, int Min) Range(IEnumerable<int> numbers)
            {
                int min = int.MaxValue;
                int max = int.MinValue;
                foreach (var n in numbers)
                {
                    min = (n < min) ? n : min;
                    max = (n > max) ? n : max;
                }
                return (max, min);
            }
        }
    }
}
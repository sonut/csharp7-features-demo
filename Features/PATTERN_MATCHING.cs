using System.Collections.Generic;
using static System.Console;

namespace Features
{
    public static class PATTERN_MATCHING
    {
        public static void IsExpression()
        {
            var items = new List<object>
            {
               100,
               100,
               "AA",
               "BB",
               new List<object>
               {
                   100,
                   "AAA",
                   100
               },
               "XY",
               100
            };
            WriteLine(DiceSum(items));

            int DiceSum(IList<object> values)
            {
                var sum = 0;
                foreach (var item in values)
                {
                    if (item is int val)
                        sum += val;
                    else if (item is IList<object> subList)
                        sum += DiceSum(subList);
                }
                return sum;
            }
        }
    }
}
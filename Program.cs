﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Features;
using static System.Console;

namespace csharp7
{
    class Program
    {
        static void Main(string[] args)
        {
            ForegroundColor = ConsoleColor.Yellow;
            WriteLine("============ C# 7 Features ============");
            ForegroundColor = ConsoleColor.Green;

            //1# out variable
            //OUT_VARIABLES.OutVariable("20");

            //2# Tupples
            //(2.i) [named tuple items]
            //TUPLES.NamedTuples();
            //(2.ii) [As Method return type, don't need to create a new type, local function]
            //TUPLES.TupleAsMethodReturnType();

            //3# Pattern Matching
            //(3.i) Expressions.. is and switch
            //PATTERN_MATCHING.IsExpression();

            //4# Ref locals and returns
            //REF_RETURNS.RefReturns();

            //5# Local functions 
            //VERSION1: keep iteration logic into same public method, Exception throws while iterate outside of the method
            //VERSION2: keep iteration logic into a private method, Exception throws from the public method itself, but need 
            // call always from public method as validation is done in public method
            //VERSION3: Use local function, exception throws from public method and iteration is done under public method context
            /*var resultSet = LOCAL_FUNCTIONS.IteratorMethod_V1('f', 'a');
            Console.WriteLine("iterator created");
            foreach (var thing in resultSet)
                Console.Write($"{thing}, ");*/

            //6# Generalized async return types
            //var task = Task.Run(async () => await ASYNC_OPTIMZATION.GeneralizedType());
            //task.Wait();

            //7# Numeric literal syntax improvements
            //WriteLine($"{ 0b0010_0000 }");
            //WriteLine($"{1_220 }");
            //WriteLine($"{1_220 * 2 }");

            //8# More expression-bodied members
            //EXPRESSION_BODIED.Demo();
        }
    }
}
